<?php
    if(isset($_POST['save'])){
        $file_name = $_FILES['image']['name'];
        $file_tmp = $_FILES['image']['tmp_name'];
        $ext = explode('.',$_FILES['image']['name']);
        $file_ext = strtolower(end($ext));
        $extensions = array("jpeg","jpg","png");
        if(in_array($file_ext,$extensions) == FALSE){
            echo "Extension not allowed";
        }
        else{
            move_uploaded_file($file_tmp,"class/".$file_name);
        }  
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>
<body>
    <form action="" enctype="multipart/form-data" method="post">
        <input type="file" name="image" />
        <button name="save">Save</button>
    </form>
</body>
</html>