<?php
    function division($dividend, $divisor){
        if($divisor == 0){
            throw new Exception("Please do not divide by zero");
        }
        else{
            $quot = $dividend / $divisor;
            echo "<p>$dividend / $divisor = $quot</p>";
        }
    }

    try{
        division(10,0);
    }
    catch(Exception $e){
        echo "Caught Exception : ". $e->getMessage();
    }
?>