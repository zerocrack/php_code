<?php
    include('db.php');
    $sql = "select * from store";
    $result = $conn->query($sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>
<body>
    <table>
        <tr>
            <td>Srno.</td>
            <td>Title</td>
            <td>Body</td>
        </tr>
        <?php
            while($rows = $result->fetch_assoc()){
        ?>
        <tr>
            <td><?php echo $rows['id']; ?></td>
            <td><?php echo $rows['title']; ?></td>
            <td><?php echo $rows['body']; ?></td>
        </tr>
            <?php } ?>
    </table>
</body>
</html>