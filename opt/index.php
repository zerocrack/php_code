<?php
    include('function.php');
    signup();
    signin();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="custom.css">
    <title></title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="column">
            <form action="" method="post">
            <h1><center>Signup</center></h1>
        <div>
            <label>Name</label>
            <input type="text" class="input" name="name" placeholder="Name..." required>
        </div>
        <div>
            <label>Email</label>
            <input type="email" class="input" name="email" placeholder="Email..." required>
        </div>
        <div>
            <label>Password</label>
            <input type="password" class="input" name="password" placeholder="Password..." required> 
        </div>
        <div>
            <label>Confirm Password</label>
            <input type="password" class="input" name="con_password" placeholder="Confirm Password..." required>
        </div>
        <div>
            <button class="btn" name="signup">Signup</button>
        </div>
        </form>

            </div>
            <div class="column">

            <form action="" method="post">
            <h1><center>Signin</center></h1>
        <div>
            <label>Email</label>
            <input type="email" class="input" name="email" placeholder="Email..." required>
        </div>
        <div>
            <label>Password</label>
            <input type="password" class="input" name="password" placeholder="Password..." required> 
        </div>
        <div>
            <button class="btn" name="signin">Signin</button>
        </div>
        </form>
            
            </div>    
        </div>
    </div>
</body>
</html>