<?php
    include('db.php');
    session_start();
    if(isset($_SESSION['id'])){
        echo $_SESSION['name'];
        echo "<a href='logout.php' class='btn'>Logout</a>";
    }
    else{
        header('Location:index.php');
    }
    if(isset($_POST['save'])){
        $title = $_POST['title'];
        $body = $_POST['body'];
        $sql = "insert into posts (title,body) values ('$title','$body')";
        $conn->query($sql);
        header('Location:dashboard.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="custom.css">
    <title></title>
</head>
<body>
<form method="post">
    <div class="container">
        <div>
            <h1><center>Create Post</center></h1>
            <label>Title</label>
            <input class="input" type="text" name="title" required><br>
        </div>
        <div>
        <label>Post</label>
        <textarea class="input" name="body" cols="40" rows="10" required></textarea>
        </div>
        <div>
            <button class="btn" name="save">Save</button>
        </div>
    </div>
    </form>
</body>
</html>