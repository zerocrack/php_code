<?php 
    //Indexded Array Words
    $colors = array("Green","Blue","Yellow","Red");
    sort($colors);
    print_r($colors);

    rsort($colors);
    print_r($colors);

    //Indexed Array Digits
    $digit = array(3,1,5,2,0,10,9);
    sort($digit);
    print_r($digit);

    rsort($digit);
    print_r($digit);
    
    //Associative Array
    $job = array("Ajay"=>"Graphic designer","Vijay"=>"Programmer","Raju"=>"Singer");
    asort($job);
    print_r($job);

    arsort($job);
    print_r($job);
?>