<?php
    // To start a session
    session_start();

    // To set a session
    // $_SESSION['username'] = "Sahil";
    // $_SESSION['password'] = "pass123";

    // // To Access a session
     echo $_SESSION['username'];
    //  echo $_SESSION['password'];
    // // To unset a session
    unset($_SESSION['username']);
    
    // // To destroy a session
    // session_destroy();
?>